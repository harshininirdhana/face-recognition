# FACE IDENTIFICATION

This notebook is a backend working python script to identify and verify people's faces.
Recognize and manipulate faces from Python or from the command line with the world’s simplest **face_recognition** library. Built using dlib’s state-of-the-art face recognition built with deep learning. The model has an **accuracy of 99.38%** on the Labeled Faces in the Wild benchmark. This also provides a simple face_recognition command line tool that lets you do face recognition on images from the command line.

Face Identifiaction - ![video](video.mp4)


## Technologies used
- Python3
- Face_recognition API
- JavaScript API
- Google Colab

## Acknowledgements

- Face_Recognition API - [Face_Recognition](https://pypi.org/project/face-recognition/) documentation 
- JavaScript API - This [notebook](https://colab.research.google.com/notebooks/snippets/advanced_outputs.ipynb) helped me script the camera accessing part in colab.

